// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Skills.generated.h"

UENUM(BlueprintType)
enum ESkills
{
	LINEA,
	AREA
};
/**
 * 
 */
USTRUCT(BlueprintType)
struct TOPDOWNBASE50_API FSkills : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TEnumAsByte<ESkills> mySkill;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int baseDamage;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float range;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int cost;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UClass* skillBP;

	FSkills() : mySkill(LINEA), baseDamage(10), range(10), cost(10), skillBP(nullptr) {}
	
};
