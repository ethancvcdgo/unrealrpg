// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownBase50PlayerController.h"
#include "GameFramework/Pawn.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "TablaDatosClases.h"
#include "Enem.h"
#include "Kismet/GameplayStatics.h"
#include "NiagaraSystem.h"
#include "NiagaraFunctionLibrary.h"
#include "Skills.h"
#include "TopDownBase50Character.h"
#include "Engine/World.h"

void ATopDownBase50PlayerController::BeginPlay()
{
	Super::BeginPlay();
}

ATopDownBase50PlayerController::ATopDownBase50PlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
}


void ATopDownBase50PlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);
	//xspeed = Dot(GetVelocity(), GetActorForwardVector());
	
	if(bInputPressed)
	{
		FollowTime += DeltaTime;

		// Look for the touch location
		FVector HitLocation = FVector::ZeroVector;
		FHitResult Hit;
		if(bIsTouch)
		{
			GetHitResultUnderFinger(ETouchIndex::Touch1, ECC_Visibility, true, Hit);
		}
		else
		{
			GetHitResultUnderCursor(ECC_Visibility, true, Hit);
		}
		HitLocation = Hit.Location;

		// Direct the Pawn towards that location
		APawn* const MyPawn = GetPawn();
		if(MyPawn)
		{
			FVector WorldDirection = (HitLocation - MyPawn->GetActorLocation()).GetSafeNormal();
			MyPawn->AddMovementInput(WorldDirection, 1.f, false);
		}
	}
	else
	{
		FollowTime = 0.f;
	}
	if(actMana < maxMana)
	{
		actMana++;
	}
}

void ATopDownBase50PlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	InputComponent->BindAction("SetDestination", IE_Pressed, this, &ATopDownBase50PlayerController::OnSetDestinationPressed);
	InputComponent->BindAction("SetDestination", IE_Released, this, &ATopDownBase50PlayerController::OnSetDestinationReleased);

	// support touch devices 
	InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &ATopDownBase50PlayerController::OnTouchPressed);
	InputComponent->BindTouch(EInputEvent::IE_Released, this, &ATopDownBase50PlayerController::OnTouchReleased);

	InputComponent->BindAction("Golpea", IE_Pressed, this, &ATopDownBase50PlayerController::HitAll);
	InputComponent->BindAction("GetClass", IE_Pressed, this, &ATopDownBase50PlayerController::GetClass);
	InputComponent->BindAction("Habilidad1", IE_Pressed, this, &ATopDownBase50PlayerController::Hab1);
	InputComponent->BindAction("Habilidad2", IE_Pressed, this, &ATopDownBase50PlayerController::Hab2);

}

void ATopDownBase50PlayerController::Hab1()
{
	evOnHab1.Broadcast();
	
}
void ATopDownBase50PlayerController::Hab2()
{
	evOnHab2.Broadcast();
}
void ATopDownBase50PlayerController::HitAll() {

}
void ATopDownBase50PlayerController::addXP() {
	actXP += 10;
	if(actXP >= maxXP)
	{
		lvl += 1;
		maxHP += 20;
		actHP += 20;
	}
}

void ATopDownBase50PlayerController::GetClass() {
	if(ClassDB && !miClass && !className.IsNone()){
		static const FString context = FString("Obtener clase con la C");
		FTablaDatosClases* clase = ClassDB->FindRow<FTablaDatosClases>(className, context, false);
		if(clase)
			miClass = clase;		
	}else {
		if(miClass) {
			UE_LOG(LogTemp, Warning, TEXT("%s"), *UEnum::GetValueAsString(miClass->myclass.GetValue()))
		}
	}
}

void ATopDownBase50PlayerController::OnSetDestinationPressed()
{
	// We flag that the input is being pressed
	bInputPressed = true;
	// Just in case the character was moving because of a previous short press we stop it
	StopMovement();
}

void ATopDownBase50PlayerController::OnSetDestinationReleased()
{
	// Player is no longer pressing the input
	bInputPressed = false;

	// If it was a short press
	if(FollowTime <= ShortPressThreshold)
	{
		// We look for the location in the world where the player has pressed the input
		FVector HitLocation = FVector::ZeroVector;
		FHitResult Hit;
		GetHitResultUnderCursor(ECC_Visibility, true, Hit);
		HitLocation = Hit.Location;

		// We move there and spawn some particles
		UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, HitLocation);
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, FXCursor, HitLocation, FRotator::ZeroRotator, FVector(1.f, 1.f, 1.f), true, true, ENCPoolMethod::None, true);
	}
}

void ATopDownBase50PlayerController::OnTouchPressed(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	bIsTouch = true;
	OnSetDestinationPressed();
}

void ATopDownBase50PlayerController::OnTouchReleased(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	bIsTouch = false;
	OnSetDestinationReleased();
}

void ATopDownBase50PlayerController::Hit_Implementation(int dmg) {
	//IHittable::Hit_Implementation(dmg);
	
	actHP -= dmg;
	UE_LOG(LogTemp, Warning, TEXT("Manpegao %s %d/%d"), *this->GetName(), actHP, maxHP);
	if (actHP <= 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("Muelto"));
	}
}
