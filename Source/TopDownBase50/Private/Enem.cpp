// Fill out your copyright notice in the Description page of Project Settings.


#include "Enem.h"

#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "TopDownBase50/TopDownBase50PlayerController.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Components/BoxComponent.h"
#include "TopDownBase50/TopDownBase50Character.h"

// Sets default values
AEnem::AEnem()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	hitZone = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxColl"));
	hitZone->SetupAttachment(RootComponent);

	sightConf = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("SightConf"));
	sightConf->SightRadius = 1250.f;
	sightConf->LoseSightRadius = 1300.f;
	sightConf->PeripheralVisionAngleDegrees = 90.f;
	sightConf->DetectionByAffiliation.bDetectEnemies = true;
	sightConf->DetectionByAffiliation.bDetectFriendlies = true;
	sightConf->DetectionByAffiliation.bDetectNeutrals = true;
	sightConf->SetMaxAge(.1);

	AIPerception = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AIPerception"));
	AIPerception->ConfigureSense(*sightConf);
	AIPerception->SetDominantSense(sightConf->GetSenseImplementation());
	AIPerception->OnPerceptionUpdated.AddDynamic(this, &AEnem::OnSensed);
	currVel = FVector::ZeroVector;
	moveSpeed = 2.f;
	distanceSQRT = BIG_NUMBER;
}

// Called when the game starts or when spawned
void AEnem::BeginPlay()
{
	Super::BeginPlay();
	hitZone->OnComponentBeginOverlap.AddDynamic(this, &AEnem::OnHit);
	basePos = GetActorLocation();
}

void AEnem::Hit_Implementation(int dmg) {
	//IHittable::Hit_Implementation(dmg);
	
	hp -= dmg;
	if (hp<=0)
	{
		Cast<ATopDownBase50PlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0))->addXP();
		Destroy();
	}
	 //UE_LOG(LogTemp, Warning, TEXT("Manpegao %s %d/%d"), *this->GetName(), hp, initialHp);
}

// Called every frame
void AEnem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!currVel.IsZero()) {
		newPos = GetActorLocation() + currVel + DeltaTime;

		if (backToBase) {
			double sqrt = (newPos - basePos).SizeSquared2D();
			if (sqrt < distanceSQRT) {
				distanceSQRT = sqrt;
			}
			else {
				currVel = FVector::ZeroVector;
				distanceSQRT = BIG_NUMBER;
				backToBase = false;

				SetNewRotation(GetActorForwardVector(), GetActorLocation());
			}
		}

		SetActorLocation(newPos);
	}
}

// Called to bind functionality to input
void AEnem::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}
void AEnem::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit) {

	ATopDownBase50Character* player = Cast<ATopDownBase50Character>(OtherActor);
	if (player) {
		Cast<ATopDownBase50PlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0))->Hit_Implementation(5);
	}

}

void AEnem::OnSensed(const TArray<AActor*>& upActors) {

	for (const auto& act : upActors) {
		FActorPerceptionBlueprintInfo info;
		AIPerception->GetActorsPerception(act, info);

		FVector dir;
		if (info.LastSensedStimuli[0].WasSuccessfullySensed()) {
			dir = act->GetActorLocation() - GetActorLocation();
			dir.Z = 0;

			currVel = dir.GetSafeNormal() * moveSpeed;

			SetNewRotation(act->GetActorLocation(), GetActorLocation());
		}
		else {
			dir = basePos - GetActorLocation();
			dir.Z = 0;

			if (dir.SizeSquared2D() > 1.) {
				currVel = dir.GetSafeNormal2D() * moveSpeed;
				backToBase = true;

				SetNewRotation(basePos, GetActorLocation());
			}
		}

	}

}
void AEnem::SetNewRotation(FVector target, FVector curr) {
	FVector newDir = target - curr;
	newDir.Z = 0.f;

	enemyRot = newDir.Rotation();

	SetActorRotation(enemyRot);
}



