// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Skills.h"
#include "Engine/DataTable.h"
#include "TablaDatosClases.generated.h"

UENUM(BlueprintType)
enum EClases {
	MAGO,
	GUERRERO
};
/**
 * 
 */
USTRUCT(BlueprintType)
struct TOPDOWNBASE50_API FTablaDatosClases : public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TEnumAsByte<EClases> myclass;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int initialHp;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int initialMp;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName habilidad1;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	FName habilidad2;

	FTablaDatosClases() : myclass(MAGO), initialHp(50), initialMp(0) , habilidad1("LINEA"), habilidad2("AREA"){}
	
};
