// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Hittable.h"
#include "Templates/SubclassOf.h"
#include "TablaDatosClases.h"
#include "Engine/DataTable.h"
#include "GameFramework/PlayerController.h"
#include "TopDownBase50PlayerController.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnHab1);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnHab2);
//DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnXP);

/** Forward declaration to improve compiling times */
class UNiagaraSystem;

UCLASS()
class ATopDownBase50PlayerController : public APlayerController, public IHittable
{
	GENERATED_BODY()

public:
	ATopDownBase50PlayerController();
	virtual void Hit_Implementation(int dmg) override;
	
	/** Time Threshold to know if it was a short press */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	float ShortPressThreshold;

	/** FX Class that we will spawn when clicking */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UNiagaraSystem* FXCursor;

	UPROPERTY(BlueprintAssignable)
	FOnHab1 evOnHab1;
	UPROPERTY(BlueprintAssignable)
	FOnHab2 evOnHab2;
	//UPROPERTY(BlueprintCallable, BlueprintAssignable)
	//FOnXP evOnXP;

	UPROPERTY(BlueprintReadWrite, BlueprintReadWrite, Category=DATATABLES)
	UDataTable* ClassDB;
	UPROPERTY(BlueprintReadWrite, BlueprintReadWrite, Category=DATATABLES)
	UDataTable* SkillsDB;
	UPROPERTY(BlueprintReadWrite, BlueprintReadWrite)
	FName className;
	UPROPERTY(BlueprintReadWrite, BlueprintReadWrite)
	FName skillName1;
	UPROPERTY(BlueprintReadWrite, BlueprintReadWrite)
	FName skillName2;

	FTablaDatosClases* miClass;
	FSkills* miSkills;

	//virtual void BeginPlay() override;
	void HitAll();
	void GetClass();
	void Hab1();
	void Hab2();
	void addXP();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int xspeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool run;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int actXP;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int maxXP = 30;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int lvl;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int maxHP;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int actHP;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int maxMana;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int actMana;
	
	
protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;
	
	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	virtual void BeginPlay() override;
	// End PlayerController interface

	/** Input handlers for SetDestination action. */
	void OnSetDestinationPressed();
	void OnSetDestinationReleased();
	void OnTouchPressed(const ETouchIndex::Type FingerIndex, const FVector Location);
	void OnTouchReleased(const ETouchIndex::Type FingerIndex, const FVector Location);

private:
	bool bInputPressed; // Input is bring pressed
	bool bIsTouch; // Is it a touch device
	float FollowTime; // For how long it has been pressed
};

