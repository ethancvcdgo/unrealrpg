# TopDownBase50

Developed with Unreal Engine 5

**Risketos Bàsics**

- [x] Un sistema preparat per tenir diverses classes, només se'n requereix un però ha d'estar preparat per ampliar, per exemple mag. En aquest ha de ser l'estructura de dades per a les estadístiques del jugador, vida, manà, energia, dany físic, dany màgic, etc. Això és al gust de cada joc.

- [x] Un sistema d'habilitats que permeti al jugador canviar quina habilitat fer servir, ha de tenir com a mínim 2. Tens dos habilitats, una llences una bola i l'has de xutar a l'enemic amb el moviment del jugador i un area que fa mal instantani als enemics que estiguin a dins.


- [x] Un sistema de combat que et permeti atacar, llançar habilitats i rebre mal.

- [x] Enemic que t'ataca.

- [x] Sistema d'anivellament que faci pujar la teva experiència i nivell, i a cada nivell pugin les teves estadístiques (vida)

**Risketos Opcionals**

- [x] Sistema d’animacions avançat amb BlendTrees (idle->caminar->correr)

